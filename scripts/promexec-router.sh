#!/bin/bash

# promexec-router version
DOCKER_IMAGE="registry.gitlab.com/psyhomb/promexec-router:latest"
# promexec-router port
PORT="8081"

# promexec-router configuration
export PROMEXEC_ROUTER_IPADDRESS="0.0.0.0"
export PROMEXEC_ROUTER_PORT="${PORT}"
export PROMEXEC_ROUTER_IPADDRESS_LABEL="ipaddress"
export PROMEXEC_ROUTER_DEBUG_ENABLED="false"
export PROMEXEC_SCHEMA="http"
export PROMEXEC_PORT="8080"
export PROMEXEC_PATH="api/exec"

docker run -d \
  --name promexec-router \
  --restart always \
  -e PROMEXEC_ROUTER_IPADDRESS \
  -e PROMEXEC_ROUTER_PORT \
  -e PROMEXEC_ROUTER_IPADDRESS_LABEL \
  -e PROMEXEC_ROUTER_DEBUG_ENABLED \
  -e PROMEXEC_SCHEMA \
  -e PROMEXEC_PORT \
  -e PROMEXEC_PATH \
  -p ${PORT}:${PROMEXEC_ROUTER_PORT} \
  ${DOCKER_IMAGE}
