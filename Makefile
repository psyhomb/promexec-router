BIN_NAME = promexec-router
REVISION ?= $(shell git rev-parse --short HEAD)
GO_VERSION ?= 1.14

.PHONY: build
build:
	go get -d
	CGO_ENABLED=0 go build -ldflags="-s -w -X main.binName=${BIN_NAME} -X main.version=${REVISION}" -o ${BIN_NAME}

.PHONY: build_in_docker
build_in_docker:
	docker run --rm -e REVISION -e GOOS -e GOARCH -v $(shell pwd):/usr/src/${BIN_NAME} -w /usr/src/${BIN_NAME} golang:${GO_VERSION} make

.PHONY: build_docker
build_docker: build_in_docker
	docker build --force-rm -t ${BIN_NAME}:${REVISION} . && \
	rm -f ${BIN_NAME} && \
	docker tag ${BIN_NAME}:${REVISION} ${BIN_NAME}:latest

.PHONY: run
run:
	@go run .

.PHONY: help
help:
	@echo "build                - Compile go code and provide binary for ${GOOS}/${GOARCH} OS"
	@echo "build_in_docker      - Compile go code inside of docker container and provide binary for ${GOOS}/${GOARCH} OS"
	@echo "build_docker         - Compile go code and build docker image for ${GOOS}/${GOARCH} OS"
	@echo "run                  - Compile and run go code"
