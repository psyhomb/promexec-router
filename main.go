package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"

	"github.com/gin-gonic/gin"
)

// Program name and version
var (
	binName string
	version string
)

// Environment variables
var (
	ginMode        string
	ipAddress      string
	ipAddressLabel string
	port           string
	promexecSchema string
	promexecPort   string
	promexecPath   string
	debugEnabled   bool
)

func init() {
	log.Printf("Running %v rev %v", binName, version)

	var err error

	port, _ = os.LookupEnv("PROMEXEC_ROUTER_PORT")
	if len(port) == 0 {
		port = "8081"
		log.Printf("[INFO] PROMEXEC_ROUTER_PORT environment variable not defined, falling back to default value %v", port)
	}

	ipAddress, _ = os.LookupEnv("PROMEXEC_ROUTER_IPADDRESS")
	if len(ipAddress) == 0 {
		ipAddress = "0.0.0.0"
		log.Printf("[INFO] PROMEXEC_ROUTER_IPADDRESS environment variable not defined, falling back to default value %v", ipAddress)
	}

	ipAddressLabel, _ = os.LookupEnv("PROMEXEC_ROUTER_IPADDRESS_LABEL")
	if len(ipAddressLabel) == 0 {
		ipAddressLabel = "ipaddress"
		log.Printf("[INFO] PROMEXEC_ROUTER_IPADDRESS_LABEL environment variable not defined, falling back to default value %v", ipAddressLabel)
	}

	v, _ := os.LookupEnv("PROMEXEC_ROUTER_DEBUG_ENABLED")
	debugEnabled, err = strconv.ParseBool(v)
	if len(v) == 0 || err != nil {
		debugEnabled = false
		log.Printf("[INFO] PROMEXEC_ROUTER_DEBUG_ENABLED environment variable not defined, falling back to default value %t", debugEnabled)
	}

	promexecSchema, _ = os.LookupEnv("PROMEXEC_SCHEMA")
	if len(promexecSchema) == 0 {
		promexecSchema = "http"
		log.Printf("[INFO] PROMEXEC_SCHEMA environment variable not defined, falling back to default value %v", promexecSchema)
	}

	promexecPort, _ = os.LookupEnv("PROMEXEC_PORT")
	if len(promexecPort) == 0 {
		promexecPort = "8080"
		log.Printf("[INFO] PROMEXEC_PORT environment variable not defined, falling back to default value %v", promexecPort)
	}

	promexecPath, _ = os.LookupEnv("PROMEXEC_PATH")
	if len(promexecPath) == 0 {
		promexecPath = "api/exec"
		log.Printf("[INFO] PROMEXEC_PATH environment variable not defined, falling back to default value %v", promexecPath)
	}

	ginMode, _ = os.LookupEnv("GIN_MODE")
	if len(ginMode) == 0 {
		ginMode = "release"
	}
}

func main() {
	gin.SetMode(ginMode)
	r := gin.Default()

	execPath := "api/route"
	r.POST(execPath, res)

	tcpIPSocket := fmt.Sprintf("%v:%v", ipAddress, port)
	r.Run(tcpIPSocket)
}

// Generate HTTP JSON response
func res(c *gin.Context) {
	var (
		req         ReqJSONBody // Raw request body received from client
		reqIP       ReqJSONBody // Aggregated request body per IP that will be sent to upstream
		alertsPerIP []ReqJSONBody
		uniqIPs     []string
		ipaddress   string
	)

	// Get raw body data that will be routed to backed ipaddress
	reqBody, err := c.GetRawData()
	if err != nil {
		log.Printf("[ERROR] %v", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	// Unmarshal HTTP request JSON body to req struct
	err = req.unmarshaler(reqBody)
	if err != nil {
		log.Printf("[ERROR] JSON unmarshal failed due to error: %v", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	// Make JSON request body compact and create a log
	if debugEnabled {
		reqBody, _ = compactor(reqBody)
		log.Printf("[DEBUG] Request JSON body: %v", string(reqBody))
	}

	// Aggregate alerts per IP
	for _, a := range req.Alerts {
		// Request will be routed upstream only if regex matches value of specified label (e.g. node, instance...)
		if isKeyInMap("promexecMatcherLabel", a.Labels) && isKeyInMap(a.Labels["promexecMatcherLabel"], a.Labels) {
			// Do not route upstream unless match
			if isKeyInMap("promexecMatcherRegex", a.Labels) {
				if !isMatchRegex(a.Labels[a.Labels["promexecMatcherLabel"]], a.Labels["promexecMatcherRegex"]) {
					continue
				}
			}

			// Do not route upstream if match
			if isKeyInMap("promexecMatcherRegexNot", a.Labels) {
				if isMatchRegex(a.Labels[a.Labels["promexecMatcherLabel"]], a.Labels["promexecMatcherRegexNot"]) {
					continue
				}
			}
		}

		// Check if unique IP already in the list
		if !isKeyInMap(ipAddressLabel, a.Labels) {
			log.Printf("[ERROR] Mandatory label %v not found in alert object with alertname: %v", ipAddressLabel, a.Labels["alertname"])
			continue
		}
		ipaddress = a.Labels[ipAddressLabel]
		if isInSlice(ipaddress, uniqIPs) {
			continue
		}

		// Generate and marshal aggregated request for upstream's IP
		reqIP = req.genReqForIP(ipaddress)
		alertsPerIP = append(alertsPerIP, reqIP)

		// Add unique IP in the list
		uniqIPs = append(uniqIPs, ipaddress)
	}

	// Iterate through list of aggregated alerts and send them to all upstreams concurrently
	chBufSize := len(alertsPerIP)
	ch := make(chan string, chBufSize)
	wg := &sync.WaitGroup{}
	wg.Add(chBufSize)
	for _, apIP := range alertsPerIP {
		go apIP.sendUpstream(ch, wg)
	}

	// Collect and log messages coming from channel
	for i := 0; i < chBufSize; i++ {
		log.Println(<-ch)
	}

	// Close the channel and return to main
	close(ch)
	wg.Wait()

	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
	})
}
