promexec-router
===============

<div align="center">

![promexec_logo_final](/uploads/21e85a3c4be234b9f6bfd028f83c2a7a/promexec_logo_final.png)
</div>

About
-----

Prometheus Executor Router (`promexec-router`) is component of [promexec](https://gitlab.com/psyhomb/promexec) stack that's made to work as [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/) receiver.  
`promexec-router` will aggregate all received alerts per IP address, generate URL with `ipaddress` as host and concurrently send those alerts to upstreams.

Example request JSON body received from [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/):

```json
{
  "receiver": "promexec-router",
  "status": "firing",
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "disk_usage_critical",
        "correlate": "disk_usage_warning,disk_usage_critical",
        "device": "/dev/sda1",
        "event": "Disk Usage server1:/",
        "fstype": "ext4",
        "instance": "server1",
        "job": "node-exporter",
        "monitor": "prometheus",
        "mountpoint": "/",
        "node": "server1",
        "ipaddress": "10.0.0.1",
        "promexecCommand": "promexec-scriptname.sh",
        "promexecSnooze": "900",
        "service": "node",
        "severity": "critical",
        "timeout": "300"
      },
      "annotations": {
        "summary": "Disk usage for mountpoint server1:/ is at 91%",
        "value": "91%"
      },
      "startsAt": "2019-11-04T17:52:19.231805866Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://10.0.0.10:9090/graph?g0.expr=instance%3Adisk_usage%3Apercent+%3E%3D+90&g0.tab=1"
    },
    {
      "status": "firing",
      "labels": {
        "alertname": "disk_usage_critical",
        "correlate": "disk_usage_warning,disk_usage_critical",
        "device": "/dev/sda1",
        "event": "Disk Usage server2:/",
        "fstype": "ext4",
        "instance": "server2",
        "job": "node-exporter",
        "monitor": "prometheus",
        "mountpoint": "/",
        "node": "server2",
        "ipaddress": "10.0.0.2",
        "promexecCommand": "promexec-scriptname.sh",
        "promexecSnooze": "900",
        "service": "node",
        "severity": "critical",
        "timeout": "300"
      },
      "annotations": {
        "summary": "Disk usage for mountpoint server2:/ is at 92%",
        "value": "92%"
      },
      "startsAt": "2019-11-04T17:52:19.231805866Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://10.0.0.10:9090/graph?g0.expr=instance%3Adisk_usage%3Apercent+%3E%3D+90&g0.tab=1"
    }
  ],
  "groupLabels": {},
  "commonLabels": {
    "job": "node-exporter",
    "monitor": "prometheus",
    "promexecSnooze": "900",
    "service": "node",
    "severity": "critical",
    "timeout": "300"
  },
  "commonAnnotations": {},
  "externalURL": "http://10.0.0.10:9093",
  "version": "4",
  "groupKey": "{}/{event=~\"^(?:.*)$\",ipaddress=~\"^(?:^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$)$\",promexecCommand=~\"^(?:.*)$\",promexecSnooze=~\"^(?:^[0-9]+$)$\"}:{}"
}
```

Using example request received from [Alertmanager](https://prometheus.io/docs/alerting/alertmanager/) and with default configuration, `promexec-router` will generate 2 URLs:

- `http://10.0.0.1/api/exec`
- `http://10.0.0.2/api/exec`

for first URL it will aggregate all alerts per `10.0.0.1` IP address (in this example for sake of simplicity we only have one alert per IP but also multiple alerts can exist in the list as well) and send it to upstream [promexec](https://gitlab.com/psyhomb/promexec) API running on remote instance. Same will be done for second URL and both/multiple requests will be sent concurrently to upstreams.

Aggregated request sent to first URL (`http://10.0.0.1/api/exec`) will look similar to this:

```json
{
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "disk_usage_critical",
        "correlate": "disk_usage_warning,disk_usage_critical",
        "device": "/dev/sda1",
        "event": "Disk Usage server1:/",
        "fstype": "ext4",
        "instance": "server1",
        "job": "node-exporter",
        "monitor": "prometheus",
        "mountpoint": "/",
        "node": "server1",
        "ipaddress": "10.0.0.1",
        "promexecCommand": "promexec-scriptname.sh",
        "promexecSnooze": "900",
        "service": "node",
        "severity": "critical",
        "timeout": "300"
      },
      "annotations": {
        "summary": "Disk usage for mountpoint server1:/ is at 91%",
        "value": "91%"
      },
      "startsAt": "2019-11-04T17:52:19.231805866Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://10.0.0.10:9090/graph?g0.expr=instance%3Adisk_usage%3Apercent+%3E%3D+90&g0.tab=1"
    }
  ]
}
```

These aggregated alerts can also be sent to any other API running on `ipaddress` if needed, but if you are using `promexec` as upstream there are several labels that have to be passed to `promexec` API:

- `promexecCommand`
- `promexecSnooze`
- `promexecSnoozeResolved`

More about these labels you will find in [promexec](https://gitlab.com/psyhomb/promexec#prometheus) documentation.

Configuration
-------------

### promexec-router

| Env Keys                          | Env (default) Values | Mandatory | Description                                                             |
|:----------------------------------|:---------------------|:----------|:------------------------------------------------------------------------|
| `PROMEXEC_ROUTER_IPADDRESS`       | 0.0.0.0              | no        | Bind to IP address                                                      |
| `PROMEXEC_ROUTER_PORT`            | 8081                 | no        | Bind to port                                                            |
| `PROMEXEC_ROUTER_IPADDRESS_LABEL` | ipaddress            | no        | Alert rule label that stores IP address of promexec instance            |
| `PROMEXEC_ROUTER_DEBUG_ENABLED`   | false                | no        | If true it will log full JSON request body received from Alertmanager   |
| `PROMEXEC_SCHEMA`                 | http                 | no        | Specify schema for upstream URL (http or https)                         |
| `PROMEXEC_PORT`                   | 8080                 | no        | Specify port for upstream URL (`promexec` default port is 8080)         |
| `PROMEXEC_PATH`                   | api/exec             | no        | Specify path for upstream URL (`promexec` default path is api/exec)     |

`promexec-router` API has only one HTTP path `api/router` and it supports only HTTP `POST` method.

```bash
curl -v -sSL -X POST -d @alerts.json http://localhost:8081/api/route
```

### Alertmanager

[Alertmanager](https://prometheus.io/docs/alerting/alertmanager/) routes and receivers configuration should look similar to this:

```yaml
route:
  receiver: default
  group_wait: 1m
  group_interval: 1m
  repeat_interval: 1m
  routes:
  - receiver: promexec-router
    continue: true
    match_re:
      ipaddress: ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$
      promexecCommand: .*
      promexecSnooze: ^[0-9]+$
  - receiver: default
    group_by: ['...']
    continue: true

receivers:
- name: promexec-router
  webhook_configs:
  - url: 'http://172.17.0.1:8081/api/route'
    send_resolved: true
- name: default
  webhook_configs:
  - url: 'https://alerta.example.com/api/webhooks/prometheus?api-key=***********'
    send_resolved: true
```

**Note:** IP address specified in webhooks url for `promexec-router` receiver is docker's bridge IP => `http://172.17.0.1:8081/api/route`

### Prometheus

Alert rule labels:

- promexec-router
  - Required
    - `ipaddress`
  - Optional
    - `promexecMatcherLabel`
    - `promexecMatcherRegex` (include)
    - `promexecMatcherRegexNot` (exclude)
- promexec
  - Required
    - `promexecCommand`
    - `promexecSnooze`
  - Optional
    - `promexecSnoozeResolved`

Alert rule example:

```yaml
groups:
  ### Disk Usage
  - name: disk_usage
    rules:
      - alert: disk_usage_warning
        expr: 80 < instance:disk_usage:percent < 90
        for: 0s
        labels:
          instance: '{{ $labels.node }}'
          service: node
          event: 'Disk Usage {{ $labels.node }}:{{ $labels.mountpoint }}'
          correlate: disk_usage_warning,disk_usage_critical
          severity: warning
        annotations:
          summary: 'Disk usage for mountpoint {{ $labels.node }}:{{ $labels.mountpoint }} is at {{ $value }}%'
          value: '{{ $value }}%'

      - alert: disk_usage_critical
        expr: instance:disk_usage:percent >= 90
        for: 0s
        labels:
          instance: '{{ $labels.node }}'
          service: node
          event: 'Disk Usage {{ $labels.node }}:{{ $labels.mountpoint }}'
          ### promexec-router labels
          ipaddress: '{{ $labels.ipaddress }}'
          promexecMatcherLabel: 'instance'
          promexecMatcherRegex: '^server[0-9]*'
          promexecMatcherRegexNot: '^(server5|server6)$'
          ### promexec labels
          promexecCommand: 'promexec-scriptname.sh'
          promexecSnooze: 900
          promexecSnoozeResolved: false
          ### END
          correlate: disk_usage_warning,disk_usage_critical
          severity: critical
        annotations:
          summary: 'Disk usage for mountpoint {{ $labels.node }}:{{ $labels.mountpoint }} is at {{ $value }}%'
          value: '{{ $value }}%'
```

**Note:** If `promexecMatcherLabel`, `promexecMatcherRegex` and `promexecMatcherRegexNot` labels are used, requests will be routed to the upstreams only if there is regex match per specified label (`promexecMatcherLabel`) in case `promexecMatcherRegex` is used or if there is no regex match in case `promexecMatcherRegexNot` label is used, otherwise alerts will be routed to all upstreams for which alerts were triggered, nevertheless scripts (`promexecCommand`) will be executed only on the upstreams that are properly configured, in other words, they have proper scripts with proper permissions installed on the upstream system and all scripts are already added in the allowed list (see [promexec](https://gitlab.com/psyhomb/promexec) documentation).

Build
-----

Build docker image with `promexec-router` binary

```bash
make build_docker
```

Run
---

Run [promexec-router](./scripts/promexec-router.sh) in docker container with default configuration

```bash
docker run -d \
  --name promexec-router \
  --restart always \
  -e PROMEXEC_ROUTER_IPADDRESS="0.0.0.0" \
  -e PROMEXEC_ROUTER_PORT="8081" \
  -e PROMEXEC_ROUTER_IPADDRESS_LABEL="ipaddress" \
  -e PROMEXEC_ROUTER_DEBUG_ENABLED="false" \
  -e PROMEXEC_SCHEMA="http" \
  -e PROMEXEC_PORT="8080" \
  -e PROMEXEC_PATH="api/exec" \
  -p 8081:8081 \
  registry.gitlab.com/psyhomb/promexec-router:latest
```
