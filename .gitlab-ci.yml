### Global variables
variables:
  SERIAL: 2022040800
  GO_VERSION: '1.18'
  GOOS: 'linux'
  GOARCH: 'amd64'

### List of stages
stages:
  - build
  - release

### Shared content
.shared:
  tags:
    - gitlab-org

### Global cache
# Pass small bits of information between jobs
cache:
  key: ${CI_COMMIT_TAG}
  paths:
    - project_cache/

### Jobs
# Build Go Binary
build_go_binary:
  image: golang:${GO_VERSION}
  stage: build
  extends: .shared
  script:
    # Environment variables
    - export REVISION="${CI_COMMIT_TAG}"
    # Save job id
    - mkdir project_cache
    - echo ${CI_JOB_ID} > project_cache/previous_job_id.txt
    # Build Go binary and create an artifact
    - mkdir target
    - make build
    - tar czvf target/${CI_PROJECT_NAME}-${REVISION}-${GOOS}-${GOARCH}.tar.gz ${CI_PROJECT_NAME}
  artifacts:
    # The value of expire_in is an elapsed time in seconds, unless a unit is provided
    # https://docs.gitlab.com/ee/ci/yaml/README.html#artifactsexpire_in
    #expire_in: 1h
    paths:
      - target/*.tar.gz
  only:
    refs:
      - tags

# Release Go Binary
release_go_binary:
  image: golang:${GO_VERSION}
  stage: release
  extends: .shared
  dependencies: []
  script:
    # Environment variables
    - export PREVIOUS_JOB_ID=$(cat project_cache/previous_job_id.txt)
    # Create a release
    - |
      curl -sSL -X POST \
        -H 'Content-Type: application/json' \
        -H "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
        -d '{
          "name": "'"${CI_COMMIT_TAG}"'",
          "tag_name": "'"${CI_COMMIT_TAG}"'",
          "description": "'"New ${CI_PROJECT_NAME} release"'",
          "assets": {
            "links": [
              {
                "name": "'"${CI_PROJECT_NAME}-${CI_COMMIT_TAG}-${GOOS}-${GOARCH}.tar.gz"'",
                "url": "'"https://gitlab.com/${GITLAB_USER_LOGIN}/${CI_PROJECT_NAME}/-/jobs/${PREVIOUS_JOB_ID}/artifacts/raw/target/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}-${GOOS}-${GOARCH}.tar.gz"'"
              }
            ]
          }
        }' \
      "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases" > /dev/null
  only:
    refs:
      - tags

# Release Docker Image
release_docker_image:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  stage: release
  extends: .shared
  dependencies:
    - build_go_binary
  script:
    # Extract binary from the artificat
    - tar xzvf target/${CI_PROJECT_NAME}-${CI_COMMIT_TAG}-${GOOS}-${GOARCH}.tar.gz
    # Build docker image with Kaniko and push it to the docker registry
    - |
      cat > /kaniko/.docker/config.json <<EOF
      {
        "auths": {
          "${CI_REGISTRY}": {
            "username": "${CI_REGISTRY_USER}",
            "password": "${CI_REGISTRY_PASSWORD}"
          }
        }
      }
      EOF
    - |
      if echo ${CI_COMMIT_TAG} | egrep -q "^[0-9]*\.[0-9]*\.[0-9]*$"; then
        OPTIONS="--destination ${CI_REGISTRY_IMAGE}:latest"
      fi
    - |
      /kaniko/executor \
        --context ${CI_PROJECT_DIR} \
        --dockerfile ${CI_PROJECT_DIR}/Dockerfile \
        --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} \
        ${OPTIONS}
  only:
    refs:
      - tags
