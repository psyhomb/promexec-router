package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"sync"
)

// ReqJSONBody struct will be used to unmarshal JSON request body
type ReqJSONBody struct {
	Alerts []Alert `json:"alerts"`
}

// Alert struct holds collection of labels coming from alert rule definitions
type Alert struct {
	Labels       map[string]string `json:"labels"`
	Annotations  map[string]string `json:"annotations"`
	Status       string            `json:"status"`
	StartsAt     string            `json:"startsAt"`
	EndsAt       string            `json:"endsAt"`
	GeneratorURL string            `json:"generatorURL"`
}

// Send aggregated alerts to upstream
func (r ReqJSONBody) sendUpstream(ch chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()

	reqBodyForIP, _ := r.marshaler()
	ipaddress := r.Alerts[0].Labels[ipAddressLabel]

	url := fmt.Sprintf(
		"%v://%v:%v/%v",
		promexecSchema, ipaddress, promexecPort, promexecPath,
	)
	res, err := http.Post(url, "application/json", bytes.NewBuffer(reqBodyForIP))
	if err != nil {
		msg := fmt.Sprintf("[ERROR] Failed to send HTTP request to upstream due to error: %v", err)
		ch <- msg

		return
	}
	defer res.Body.Close()

	msg := fmt.Sprintf("[INFO] Request routed to URL: %v", url)
	ch <- msg
}

// Generate body payload for upstream's IP
func (r ReqJSONBody) genReqForIP(ip string) ReqJSONBody {
	var ur ReqJSONBody

	for _, a := range r.Alerts {
		if a.Labels[ipAddressLabel] == ip {
			ur.Alerts = append(ur.Alerts, a)
		}
	}

	return ur
}

// JSON unmarshaler
func (r *ReqJSONBody) unmarshaler(rb []byte) error {
	err := json.Unmarshal(rb, &r)
	if err != nil {
		return err
	}

	return nil
}

// JSON marshaler
func (r *ReqJSONBody) marshaler() ([]byte, error) {
	jsonBody, err := json.Marshal(r)
	if err != nil {
		return nil, err
	}

	return jsonBody, nil
}

// JSON Compactor
func compactor(b []byte) ([]byte, error) {
	buffer := new(bytes.Buffer)
	err := json.Compact(buffer, b)
	if err != nil {
		return nil, err
	}

	return buffer.Bytes(), nil
}

// Check if element in slice
func isInSlice(v string, s []string) bool {
	for _, e := range s {
		if e == v {
			return true
		}
	}

	return false
}

// Check if key in map
func isKeyInMap(v string, m map[string]string) bool {
	for k := range m {
		if k == v {
			return true
		}
	}

	return false
}

// Check if regex matches string
func isMatchRegex(s, r string) bool {
	re := regexp.MustCompile(r)

	if re.MatchString(s) {
		return true
	}

	return false
}
