FROM scratch

LABEL maintainer="milosbuncic@gmail.com"

COPY promexec-router /

CMD ["/promexec-router"] 
